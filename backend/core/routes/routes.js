const controllers = require('../controllers');
const { app } = require('steplix-microservice/server');
const { database } = require('steplix-microservice');
const pkg = require('../../package.json');
const { health } = require('steplix-microservice/server/routes');

health(app, { pkg, database });

app.get('/rates', controllers.rates.list);
app.get('/rates/:symbol', controllers.rates.get);
app.post('/rates', controllers.rates.create);
app.get('/currencies', controllers.currencies.list);

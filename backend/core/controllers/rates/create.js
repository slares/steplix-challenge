const Base = require('steplix-microservice/server/routes');
const { errors, database } = require('steplix-microservice');
const { validator } = require('steplix-microservice/helpers');
const _ = require('lodash');

class Route extends Base {
  validate(req) {
    if (_.isEmpty(req.body)) {
      throw new errors.BadRequest('Bad request. Please set body parameters');
    }
    if (!validator.isNumber(req.body.value)) {
      throw new errors.BadRequest('Bad request. Please use "value" on body parameters');
    }
    if (!validator.isInteger(req.body.id_currency)) {
      throw new errors.BadRequest('Bad request. Please use "id_currency" on body parameters');
    }
  }

  handler(req) {
    const data = {
      value: req.body.value,
      id_currency: req.body.id_currency,
    };
    return database.models.Rates.create(data).catch(e => {
      throw new errors.InternalServerError(e);
    });
  }
}

module.exports = new Route('Rates').handlerize();

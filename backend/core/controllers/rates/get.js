const { errors, database } = require('steplix-microservice');
const Base = require('steplix-microservice/server/routes');

class Route extends Base {
  handler(req) {
    const symbol = req.params.symbol.toLowerCase();

    const options = {
      with: ['currency'],
      order: [['created_at', 'DESC']],
    };
    return database.models.Rates.find(options)
      .then(model => {
        if (!model) {
          throw new errors.NotFound(`Not found model ${id}`);
        }
        return model.filter(model => model.currency.symbol.toLowerCase() === symbol)[0];
      })
      .catch(e => {
        throw new errors.InternalServerError(e);
      });
  }
}

module.exports = new Route('Rates').handlerize();

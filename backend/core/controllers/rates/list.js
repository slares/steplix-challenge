const Base = require('steplix-microservice/server/routes/list');
const { errors, database } = require('steplix-microservice');

class Route extends Base {
  handler() {
    return database.models.Rates.query(
      'SELECT `rates`.`id`, `rates`.`id_currency`, `rates`.`value`, `rates`.`created_at`, `rates`.`id_currency`, `currencies`.`id` AS `currency.id`, `currencies`.`description` AS `currency.description`, `currencies`.`symbol` AS `currency.symbol` FROM `rates` INNER JOIN `currencies` on `rates`.`id_currency` = `currencies`.`id` ORDER BY `created_at` ASC',
      {
        nest: true,
      },
    )
      .then(model => {
        if (!model) {
          throw new errors.NotFound(`Not found model ${id}`);
        }
        let uniqueRatesArray = [...new Map(model.map(item => [item['id_currency'], item])).values()];
        return uniqueRatesArray;
      })
      .catch(e => {
        throw new errors.InternalServerError(e);
      });
  }
}

module.exports = new Route('Rates').handlerize();

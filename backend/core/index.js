'use strict';
const { app, listen } = require('steplix-microservice/server');
const { errors } = require('steplix-microservice/server/routes');
require('./routes');

errors(app);

listen(app);

module.exports = app;

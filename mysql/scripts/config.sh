#!/bin/bash

# Constants

## Environment
environment="local"

## MySQL
host="host.docker.internal" # execution outside docker
# host="localhost" # execution inside docker or local installation of mysql
port="3306"
username="root"
password="AHV90ry4090420202090??"

## Database
databaseDirname="${src}"
database="cryptocurrencies"

## Script
dummy=true
views=false
basedata=true
constraints=true
production=false
testing=false
staging=false
verbose=false

## Colors
bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)
purple=$(tput setaf 171)
red=$(tput setaf 1)
green=$(tput setaf 76)
tan=$(tput setaf 3)
blue=$(tput setaf 38)

#!/bin/bash

# Constants
# src=.
src=$(pwd)

# Imoprts
source "${src}/scripts/config.sh"
source "${src}/scripts/functions.sh"
source "${src}/scripts/arguments.sh"

# Source code
e_header "Installing Database ${database} on ${environment}"

installDatabase
installFolder "tables"
installFolder "constraints"
installFolder "basedata"
# if [ $constraints == true ]; then installFolder "constraints"; fi
# if [ $basedata == true ]; then installFolder "basedata"; fi
# if [ $views == true ]; then installFolder "views"; fi
# if [ $dummy == true ]; then installFolder "dummy"; fi

# Bye!
e_bye

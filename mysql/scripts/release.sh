#!/bin/bash

# Constants
src=.

# Imoprts
source "${src}/scripts/config.sh"
source "${src}/scripts/functions.sh"
source "${src}/scripts/arguments.sh"

for i; do
    PARAM=`echo ${i} | awk -F= '{print $1}'`
    VALUE=`echo ${i} | awk -F= '{print $2}'`
    case "$PARAM" in
             --release)    release=$VALUE;;
        -u | --up)         action="up";;
        -d | --down)       action="down";;
    esac
done

if [ -z $action ]; then
    action="up"
fi

if [ -z $release ]; then
    e_error "Please select a release\n";
    usage;
    exit 1;
fi

# Source code
e_header "Release ${release} for database ${database} on ${environment}"

executeRelease $release $action

# Bye!
e_bye

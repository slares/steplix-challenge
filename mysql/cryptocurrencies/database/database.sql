DROP DATABASE IF EXISTS `cryptocurrencies`;
CREATE DATABASE `cryptocurrencies` CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `cryptocurrencies`;

DROP USER IF EXISTS `cryptocurrencies`;
CREATE USER `cryptocurrencies`;

-- GRANT ALL PRIVILEGES ON cryptocurrencies.* To 'cryptocurrencies'@'localhost' IDENTIFIED BY 'Steplix4999!';
-- GRANT ALL PRIVILEGES ON cryptocurrencies.* To 'cryptocurrencies'@'%' IDENTIFIED BY 'Steplix4999!';


USE `cryptocurrencies` ;

# ================================================================================================

DROP TABLE IF EXISTS `currencies`;

CREATE TABLE `currencies` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NOT NULL,
  `symbol` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`)
  ) ENGINE = InnoDB;

# ================================================================================================

DROP TABLE IF EXISTS `rates`;

CREATE TABLE `rates` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `value` REAL NOT NULL,
  `id_currency` INT NOT NULL,
  PRIMARY KEY (`id`)
    ) ENGINE = InnoDB;


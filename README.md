


## Microservice

## Download

### Source code

```shell
$ git clone git@gitlab.com:slares/steplix-challenge.git
$ cd backend
$ npm install
```

## How is it used?

### Run

Create a .env file or export the required environment variables based on the .env.\* files and then run one of the following commands:

```shell
# Development
npm run dev
# Development - Watch changes
npm run dev:watch

# Production
npm start

# Docker
docker-compose up -d --build
```

## MySql DB

## Download

### Source code

```shell
$ git clone git@gitlab.com:slares/steplix-challenge.git

```

## How is it used?

### Run

```
$ cd mysql
docker-compose up -d --build
$ ./scripts/install.sh --cryptocurrencies
```
